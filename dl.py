import os
import re

def extract_series_and_numbers(input_text):
    # Define a regular expression pattern to match series names and parenthesized numbers
    pattern = r'^\s*.\s(.*?)\s*\([*]?[*]?(\d+)[*]?[*]?\)\s*$'
        
    # Split the input text into lines and process each line
    lines = input_text.split('\n')
    series_and_numbers = {}
    
    # if is single series, just grab series name/number from first line
    if is_series(input_text):
        match = re.match(pattern, lines[0])
        bundle_name = match.group(1)
        series_number = int(match.group(2))
        series_and_numbers[bundle_name] = series_number
    else:    
        if lines[0].startswith('-'):
            lines=lines[1:] # get rid of leading "-----" line
            bundle_name=lines[0][2:] # grab bundle name ignoring leading "**"
        else:
            bundle_name = lines[0].strip()
                
        for line in lines:
            match = re.match(pattern, line)
            if match:
                series_name = match.group(1)
                series_number = int(match.group(2))
                if series_name not in series_and_numbers:
                    series_and_numbers[series_name] = series_number
    return series_and_numbers, bundle_name

def find_common_series_and_sum(input_text1, input_text2):
    # Extract series and numbers from both input texts
    series_and_numbers1, bundle_name1 = extract_series_and_numbers(input_text1)
    series_and_numbers2, bundle_name2 = extract_series_and_numbers(input_text2)
    # Find the common series names
    common_series_names = set(series_and_numbers1.keys()).intersection(series_and_numbers2.keys())
    
    # Calculate the sum of numbers for the common series
    total_sum = sum(series_and_numbers1[name] for name in common_series_names)
    
    return common_series_names, total_sum, bundle_name1, bundle_name2

def find_common_series_and_sums_in_current_folder(dictx):
    # Get the script's file name
    script_filename = os.path.basename(__file__)
    
    # Files to be excluded (script and README.md)
    excluded_files = [script_filename, "README.md"]
    
    # Get a list of all files in the current working directory
    files = os.listdir()
    
    # Exclude specified files from the list of files
    input_files = [file for file in files if os.path.isfile(file) and file not in excluded_files]
    
    # Initialize a dictionary to store common series names and sums for each pair of files
    common_series_sums = {}
    
    # Initialize a variable to store the total sum
    total_sum = 0
    
    new_dict = dictx.copy()
    
    # Iterate through pairs of input files
    for i in range(len(input_files)):
        for j in range(i + 1, len(input_files)):
            file1 = input_files[i]
            file2 = input_files[j]
            
            # Read the contents of the files
            with open(file1, 'r') as f1, open(file2, 'r') as f2:
                text1 = f1.read()
                text2 = f2.read()
            
            # Calculate common series, sums, and bundle names for the pair
            common_series, sum_of_numbers, bundle_name1, bundle_name2 = find_common_series_and_sum(text1, text2)
            
            # Check if the sum is non-zero before storing the result
            if sum_of_numbers != 0:
                # Store the result in the dictionary3
                pair_name = f"{bundle_name1} — {bundle_name2}"
                common_series_sums[pair_name] = {
                    "Common Series Names": sorted(common_series),
                    "Overlap": sum_of_numbers
                }
                
                # Add the sum to the total sum
                total_sum += sum_of_numbers
                
                # Update individual bundle dicts
                new_dict[bundle_name1] = (new_dict[bundle_name1][0], new_dict[bundle_name1][1] + sum_of_numbers)
                new_dict[bundle_name2] = (new_dict[bundle_name2][0], new_dict[bundle_name2][1] + sum_of_numbers)

    return common_series_sums, total_sum, new_dict

def find_bundle_name_and_num_characters():
    # Get the script's file name
    
    bundles = {}
    script_filename = os.path.basename(__file__)
    
    # Files to be excluded (script and README)
    excluded_files = [script_filename, "README.md"]
    
    # Get a list of all files in the current working directory
    files = os.listdir()
    
    # Exclude specified files from the list of files
    input_files = [file for file in files if os.path.isfile(file) and file not in excluded_files]
    
    for f in input_files:
        with open(f, 'r') as f1:
            text = f1.read()
        lines = text.split('\n')
        if is_series(text): # if is single series, use series regex to extract name and save as bundle name
            pattern = r'^\s*.\s(.*?)\s*\([*]?[*]?(\d+)[*]?[*]?\)\s*$'
            match = re.match(pattern, lines[0])
            bundle_name = match.group(1)
            num_characters = int(match.group(2))
            
            # add to dict
            bundles[bundle_name] = (num_characters, 0)
        else:
            pattern = r'\d+'
            if lines[0].startswith('-'):
                lines=lines[1:] # get rid of leading "-----" line
                bundle_name=lines[0][2:] # grab bundle name ignoring leading "**"
            else:
                bundle_name = lines[0].strip()
                
            num_characters = re.search(pattern, lines[1].strip())
            
            # add to dict
            bundles[bundle_name] = (num_characters[0], 0)
            
    return bundles

def print_bundle_name_and_num_characters(dictx):
    sorted_tuples = sorted((key, value) for key, value in dictx.items())
    print("Bundles:")
    for x in sorted_tuples:
        print(f"{x[0]}: {x[1][0]} characters")

def print_overlap_by_bundle(dictx):
    sorted_tuples = sorted(((key, value) for key, value in dictx.items()), key=lambda x : x[1][1], reverse=True)
    print("Num overlaps by bundle:")
    for x in sorted_tuples:
        print(f"{x[0]}: {x[1][1]}")

def print_overlap_by_pair(details):
    sorted_tuples = sorted(((key, value) for key, value in details), key=lambda x : x[1]['Overlap'], reverse=True)
    print("Num overlaps by pair:")
    for x in sorted_tuples:
        print(f"{x[0]}: {x[1]['Overlap']}")

def is_series(input_text):
    lines = input_text.split('\n')
    return lines[0].startswith('·')
        
bundle_name_and_num_chars = find_bundle_name_and_num_characters()
common_series_sums, total_sum, dictx = find_common_series_and_sums_in_current_folder(bundle_name_and_num_chars)
print_bundle_name_and_num_characters(dictx)
print()
print_overlap_by_bundle(dictx)
print()
print_overlap_by_pair(common_series_sums.items())
print()


# Print the total sum

print()
print("Total Overlap:", total_sum)
print()
print()
print()

print("Detailed breakdown:")
# Display results for each pair of files
for pair_name, results in sorted(((key, value) for key, value in common_series_sums.items()), key=lambda x : x[1]['Overlap'], reverse=True):
    print(pair_name)
    print(results["Common Series Names"])
    print("Overlap:", results["Overlap"], "characters")
    print()

