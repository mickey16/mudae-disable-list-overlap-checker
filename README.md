## mudae disable list overlap checker

Quick script for calculating size of overlaps for mudae disable lists. Gives a breakdown of overlaps by bundle/series and for each pair of bundles/series.
You have to manually fetch the bundle info from mudae first; this script doesn't automatically grab it. Once you have that info, this script will compute all overlaps between all provided bundles/series.

## Example Output (using 15 bundles/series):
```
Bundles:
Dangan Fangan Bundgan: 194 characters
Demon Compendium: 143 characters
Digimon: 247 characters
Dragon Ball Series: 297 characters
Fighting Games: 1078 characters
First-person Shooter Games: 1071 characters
Furry: 743 characters
Isekai: 3841 characters
MOBA Games: 676 characters
Mobile Games: 10135 characters
One Piece Series: 458 characters
Pokédex: 574 characters
Ren'Py Games: 1077 characters
Sports Animanga: 2068 characters
Super Mario: 413 characters

Num overlaps by bundle:
Mobile Games: 1160
Isekai: 570
Furry: 403
MOBA Games: 290
Digimon: 189
Ren'Py Games: 170
Sports Animanga: 134
Fighting Games: 70
Super Mario: 39
Dragon Ball Series: 5
Dangan Fangan Bundgan: 0
Demon Compendium: 0
First-person Shooter Games: 0
One Piece Series: 0
Pokédex: 0

Num overlaps by pair:
Isekai — Mobile Games: 340
MOBA Games — Mobile Games: 290
Furry — Mobile Games: 279
Digimon — Isekai: 183
Mobile Games — Sports Animanga: 134
Furry — Ren'Py Games: 123
Fighting Games — Mobile Games: 68
Mobile Games — Ren'Py Games: 40
Isekai — Super Mario: 39
Isekai — Ren'Py Games: 7
Digimon — Mobile Games: 6
Dragon Ball Series — Mobile Games: 3
Dragon Ball Series — Fighting Games: 2
Furry — Isekai: 1


Total Overlap: 1515



Detailed breakdown:
Isekai — Mobile Games
['EVERSOUL', 'Fire Emblem Heroes', 'Genshin Impact', 'ISEKAI: Demon Waifu', 'Princess Connect! Re:Dive', 'Sengoku Night Blood']
Overlap: 340 characters

MOBA Games — Mobile Games
['#COMPASS', 'Arena of Valor', 'Brawl Stars', 'Dungeon Hunter Champions', 'Eternal Return: Black Survival', 'Extraordinary Ones', 'Mobile Legends', 'Vainglory']
Overlap: 290 characters

Furry — Mobile Games
['AnotherEidos Of Dragon Vein R', 'Beek - Familiar Spirit', 'GYEE', 'Live A Hero', 'Tokyo Afterschool Summoners']
Overlap: 279 characters

Digimon — Isekai
['Digimon Adventure', 'Digimon Adventure V-Tamer 01', 'Digimon Frontier', 'Digimon Next', 'Digimon Savers', 'Digimon Story Sunburst & Moonlight', 'Digimon Survive', 'Digimon Tamers', 'Digimon World', 'Digimon World Next Order', 'Digimon Xros Wars']
Overlap: 183 characters

Mobile Games — Sports Animanga
['PuraOre! Pride of Orange', 'TRIBE NINE', 'Uma Musume: Pretty Derby']
Overlap: 134 characters

Furry — Ren'Py Games
['Adastra', 'After Class', 'Arches', 'Dawn Chorus', 'Dragon Island', 'Echo', "Eden's Reach", 'Extracurricular Activities', 'Far Beyond the World', 'Great Troubles', 'In Case of Emergency', 'Khemia', "Killigan's Treasure", 'Love at First Tail', 'Lustful Desires', 'Lyre', 'Minotaur Hotel', 'Nekojishi', 'Password', 'Remember the Flowers', 'Repeat', 'Shelter (VN)', 'Tavern of Spear', "Temptation's Ballad", 'Tennis Ace', 'The Human Heart', 'The Smoke Room']
Overlap: 123 characters

Fighting Games — Mobile Games
['Marvel Contest of Champions', 'Shadow Fight', 'Skullgirls', 'Smash Legends']
Overlap: 68 characters

Mobile Games — Ren'Py Games
['Locked Heart', 'One Thousand Lies', 'Seduce Me the Otome', 'The Headmaster', 'To the Edge of the Sky']
Overlap: 40 characters

Isekai — Super Mario
['Mario + Rabbids Kingdom Battle', 'Mario + Rabbids Sparks of Hope', 'Super Mario Bros. (1993)', 'Super Mario World (TV)', 'The Adventures of Super Mario Bros. 3', 'The Super Mario Bros. Movie', 'The Super Mario Bros. Super Show!']
Overlap: 39 characters

Isekai — Ren'Py Games
['Ascendant Hearts', 'Kotonoha Amrilato']
Overlap: 7 characters

Digimon — Mobile Games
['Digimon Crusader', 'Digimon Linkz', 'Digimon ReArise']
Overlap: 6 characters

Dragon Ball Series — Mobile Games
['Dragon Ball Legends']
Overlap: 3 characters

Dragon Ball Series — Fighting Games
['Dragon Ball FighterZ']
Overlap: 2 characters

Furry — Isekai
['Teraurge']
Overlap: 1 characters

```

## How to Use:

On mudae, run the following on each bundle to get a DM with a list of series in each bundle (skip if including a series, see the note on step 3 below):
```
$imabs <bundle_name> 
```

1. Copy text from DMs (include header lines with series name and num characters in bundle).

* NOTE: if the bundle is large enough, the DM will actually contain multiple DMs. When copying those, it'll sometimes include random lines with timestamps, like [1:09 PM] or something like that. Feel free to leave that in; the script will automatically skip those lines.
* NOTE2: if you use the Discord 'Copy Text' button instead of highlighting, the text will include a leading line with hyphens and some extra astericks. You can leave this in; the script will filter it out. Example input that is fine:
```
-----
**Fighting Games
(Bundle - 1078 chars)
​**
· Aggressors of Dark Kombat (**1**)
· Akatsuki Blitzkampf (**7**)
```

2.  Save the DM content into a text file. Do this for each bundle (each bundle should be in its own separate text file).
3.  Place each text file in the same folder as script.
* This folder should ONLY contain the script, this README, and text files containing bundle info.

* NOTE: If you have want to include a series (not a bundle), save the series in its own text file with a single bulletpoint with the series info, in the same format as the bulletpoints used when listing series in a bundle. You do not need the bundle/character info at the top. Example:
```
· Akatsuki Blitzkampf (7)
```



4. Once all files are in the folder containing the script, run:
```
python dl.py
```
Output will print to stdout.
 

Some example files are in this folder. Just cloning the entire repo directory and running as-is should work, as an example.
